#!/bin/bash
docker run -d -p 27017:27017 --name dev-mongo \
    -e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
    -e MONGO_INITDB_ROOT_PASSWORD=secret \
    registry.gitlab.com/b-k8s/mongo:bionic
